import random
from PIL import Image, ImageDraw, ImageShow, ImageSequence

class Designer:

    COLORS = {
                "cyan":         (0,255,255),
                "light-cyan":   (224,252,252),
                "blue":         (0,0,255),
                "red":          (255,0,0),
                "green":        (0,255,0),
                "black":        (0,0,0),
                "white":        (255,255,255),
                ## base random colors
                "random1":      (random.randint(0,256), random.randint(0,256), random.randint(0,256)),
                "random2":      (random.randint(0,256), random.randint(0,256), random.randint(0,256)),
                "random3":      (random.randint(0,256), random.randint(0,256), random.randint(0,256)),
                "random4":      (random.randint(0,256), random.randint(0,256), random.randint(0,256)),

            }

    def __init__(self, grid, nbr_frames,height, width, max_elevation=100, outline=False):
        self.grid   = grid
        self.height = height
        self.width  = width 
        self.im     = Image.new("RGBA",(self.height,self.width), "white")
        self.nbr_frames = nbr_frames
        self.max_elevation = max_elevation
        self.outline = outline

    ## Functions
    def drawPoint(self, x, y, color=(0,0,0)):
        intx = max(min(int(x), self.height-1),0)
        inty = max(min(int(y), self.width-1),0)
        self.im.putpixel((intx,inty), color)

    def drawSegment(self, x1, y1, x2, y2, color="red"):
        drw = ImageDraw.Draw(self.im, 'RGBA')
        drw.line([(x1, y1), (x2,y2)], color)

    def drawPolygon(self, vertices, fill=(125,0,0,50), outline=None):
        if (self.outline and not(outline)):
            outline = self.outline
        drw = ImageDraw.Draw(self.im, 'RGBA')
        drw.polygon(vertices, fill, outline=outline)

    def setColor(self, percent, begcolor, endcolor):
        """percent is a "slider" between begcolor and endcolor"""
        redp = begcolor[0] * percent/100 + endcolor[0] * (1-percent/100)
        grep = begcolor[1] * percent/100 + endcolor[1] * (1-percent/100)
        blup = begcolor[2] * percent/100 + endcolor[2] * (1-percent/100)
        return (int(redp), int(grep), int(blup))

    def makeImage(self, counter=0):
        """
        counter : positive int
        """

        # reset image :
        self.im     = Image.new("RGBA",(self.height,self.width), "white")

        for p in self.grid.points:
            self.drawPoint(p[0], p[1])
        
    #    for v in self.grid.vor_pts.vertices:
    #        # px : intersection point between the segment and the edge
    #        px = max(min(v[0], WIDTH-1), 0)
    #        py = max(min(v[1], WIDTH-1), 0)
    #        drawPoint(im, int(px), int(py), color=(255,0,0))

        for i in range(len(self.grid.vor_pts.regions)):
            r = self.grid.vor_pts.regions[i]
            height = self.grid.elevation[i]
            rp = []
            for x in r:
                rp.append((int(self.grid.vor_pts.vertices[x][0]), int(self.grid.vor_pts.vertices[x][1])))
            if rp:
                progression = counter/self.nbr_frames
                ## colors for animated gifs
#                if counter < self.nbr_frames/2:
#                    begcolor    = (255*2*counter/self.nbr_frames, 255*(1-2*counter/self.nbr_frames), 0)
#                    endcolor    = (0, 255*2*counter/self.nbr_frames, 255*(1-2*counter/self.nbr_frames))
#                else:
#                    begcolor    = (255*(2-2*counter/self.nbr_frames), 255*(-1+2*counter/self.nbr_frames), 0)
#                    endcolor    = (0, 255*(2-2*counter/self.nbr_frames), 255*(-1+2*counter/self.nbr_frames))
#                ## colors for a sstar night sky
#                begcolor    = (228, 252, 12)
#                endcolor    = (1, 5, 56)
#                ## colors for broken glass
#                begcolor    = self.COLORS["white"]
#                endcolor    = self.COLORS["cyan"]
#                ## random colors
#                begcolor    = (random.randint(0,256), random.randint(0,256), random.randint(0,256))
#                endcolor    = (random.randint(0,256), random.randint(0,256), random.randint(0,256))
                if progression < 0.5:
                    begcolor = self.setColor(2*progression*100, self.COLORS["random1"], self.COLORS["random2"])
                    endcolor = self.setColor(2*progression*100, self.COLORS["random3"], self.COLORS["random4"])
                else:
                    begcolor = self.setColor((2*progression-1)*100, self.COLORS["random2"], self.COLORS["random1"])
                    endcolor = self.setColor((2*progression-1)*100, self.COLORS["random4"], self.COLORS["random3"])
                fill = self.setColor(height, begcolor, endcolor)
                self.drawPolygon(rp, fill=fill)


    def makeGifChangingColors(self):
        nbr_frames = self.nbr_frames
        frames = []
        for i in range(nbr_frames):
            self.makeImage(counter=i)
            frames.append(self.im.copy())
        self.im.save("results/output.gif", save_all=True, append_images=frames[1:], duration=100)

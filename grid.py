import random
from math import sqrt
import scipy.spatial as spl

class Grid:
    points  = []
    height  = 0
    width   = 0
    pts_nbr = 0
    vor_pts = []
    elevation = []
    max_elevation = 100

    def __init__(self, height, width, pts_nbr, max_elevation, nbr_hills, max_hill_radius):
        self.height     = height
        self.width      = width
        self.pts_nbr    = pts_nbr
        self.max_elevation = max_elevation
        self.nbr_hills  = nbr_hills
        self.max_hill_radius = max_hill_radius

    def generate(self):
        for i in range(self.pts_nbr):
            self.points.append((random.randint(0,self.height-1), random.randint(0,self.width-1)))
        print("Beginning Lloyd's relaxation...")
        self.lloydRelaxation()
        self.lloydRelaxation()
        print("[OK] Lloyd's relaxation done.")
        self.vor_pts = spl.Voronoi(self.points)
        print("Setting elevations...")
        for i in range(len(self.vor_pts.regions)):
            self.elevation.append(1)

        for i in range(self.nbr_hills):
            print("Loading " + str(i/self.nbr_hills*100) +"%")
            radius = random.randint(2, self.max_hill_radius)
            self.gen_hill(radius=radius)
        print("[OK] Elevation setting done.")

    def gen_hill(self,radius=0):
        """Generate a hill !"""

        def elevateNeighbours(n1, radius, distance):
            """n1 : int
            radius : int
            distance : 1
            """
            for n2 in self.getBorderRegions(n1):
                new_height = hill_height/(distance+1)
                if self.elevation[n2] < new_height:
                    self.elevation[n2] = new_height
                    if distance <= radius:
                        elevateNeighbours(n2, radius, distance+1)

        hill_height = random.randint(0,self.max_elevation)
        r_id = random.randint(0,len(self.vor_pts.regions))
        region = self.vor_pts.regions[r_id]
        self.elevation[r_id] = hill_height

        for n1 in self.getBorderRegions(r_id):
            distance = 1
            if distance <= radius:
                elevateNeighbours(n1, radius, distance)


    def getBorderRegions(self, r_id):
        """ r_id : int
            res : int[]
        """
        res = []
        reg0 = self.vor_pts.regions[r_id]
        # we browse the list of regions
        for i in range(len(self.vor_pts.regions)):
            reg = self.vor_pts.regions[i]
            for v in reg0:
                if v in reg and not(i in res):
                    res.append(i)
        return res

    def lloydRelaxation(self):
        self.vor_pts = spl.Voronoi(self.points)
        new_pts = []
        for reg in self.vor_pts.regions:
            reg_vertices = [(self.vor_pts.vertices[x][0], self.vor_pts.vertices[x][1]) for x in reg]
            nbr = len(reg_vertices)
            if nbr == 0:
                continue
            new_x = 0
            new_y = 0
            for i in range(nbr):
                new_x += reg_vertices[i][0]
                new_y += reg_vertices[i][1]
            new_pts.append((new_x/nbr, new_y/nbr))
        self.points = new_pts



    def distance(p1, p2):
        return sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

from random import randint

from grid import Grid
from showPillow import Designer

HEIGHT  = 700
WIDTH   = 700

PTSNBR  = 2**9
NBR_HILLS   = 1
MAX_HILL_RADIUS = 8
MAX_HEIGHT  = 100
# minimum height is 0
NBR_FRAMES  = 1
OUTLINE = "black"

## Algorithm

print("Beginning generation...")

grid = Grid(HEIGHT, WIDTH, PTSNBR, max_elevation=MAX_HEIGHT, nbr_hills=NBR_HILLS, max_hill_radius=MAX_HILL_RADIUS)
grid.generate()

print("Showing image...")

designer = Designer(grid, NBR_FRAMES, HEIGHT, WIDTH, max_elevation=MAX_HEIGHT, outline=OUTLINE)
designer.makeImage()

designer.im.save("results/output.png")
designer.im.show()

designer.makeGifChangingColors()
